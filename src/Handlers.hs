{-# LANGUAGE DataKinds       #-}
{-# LANGUAGE TypeOperators   #-}
{-# LANGUAGE TemplateHaskell #-}
module Handlers where

import           Data.Aeson
import           Data.Aeson.TH
import           Servant                        ( err409
                                                , err500
                                                , ServerError
                                                )
import           Models
import           Database.Persist
import qualified Database.Sqlite as S
import           Control.Monad.Freer
import           Control.Monad.Freer.Error
import           Control.Monad.Freer.Sql

data NewUser = NewUser
  { name :: String
  , age :: Int
  }

$(deriveJSON defaultOptions ''NewUser)

-- | This is a standard api handler function to get all users from the db. The only difference is that 
getUsers :: Members '[SQL] effs => Eff effs [User]
getUsers = do
  users <- sql $ selectList [] []
  return $ entityVal <$> users

-- | Here we use the trySql action because we can expect a user with the same name to be added on creation
newUser :: Members '[SQL, Error ServerError] effs => NewUser -> Eff effs UserId
newUser NewUser {..} = do
  mUid <- trySql $ insert $ User name age
  case mUid of
    Right uid -> return uid
    Left  (S.SqliteException S.ErrorConstraint _ _ ) -> throwError err409
    _ -> throwError err500
