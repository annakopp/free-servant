module Api
  ( startApp
  , app
  )
where

import           Network.Wai
import qualified Network.Wai.Handler.Warp      as W
import           Servant
import           Models
import           Database.Persist.Sqlite
import           Control.Monad.IO.Class         ( liftIO )
import           Control.Monad.Freer
import           Control.Monad.Logger
import           Control.Monad.Except           ( ExceptT(..) )
import           Data.Function
import           Control.Monad.Freer.Error
import           Handlers
import           Control.Monad.Freer.Sql


type API = "users" :> Get '[JSON] [User]
       :<|>"users" :> ReqBody '[JSON] NewUser :> Post '[JSON] UserId


-- | Starts the server, passes a SqlBackend connection (a sqlite in-memory db) to the rest of the app
startApp :: IO ()
startApp =
  runStdoutLoggingT $ withSqliteConn ":memory:" $ \backend -> do
    runSqlConn (runMigration migrateAll) backend
    liftIO $ W.run 8080 (app backend)

-- | A regular servant server does stuff inside Handler. We're using effects so we'll need to
-- convert Eff to Handler.   
app :: SqlBackend -> Application
app backend = serve api (hoistServer api nt server)
  where nt :: Eff '[SQL, Error ServerError, IO] ~> Handler
        nt effs = mkHandler $ runEffects effs

        runEffects :: Eff '[SQL, Error ServerError, IO] a -> IO (Either ServerError a)
        runEffects effs = effs
          & runSql backend -- run & pop the SQL effect from list
          & runError -- deal with any errors that arose from prev step
          & runM -- end up in IO

        mkHandler :: IO (Either ServerError a) -> Handler a
        mkHandler = Handler . ExceptT

api :: Proxy API
api = Proxy

-- | Because of the natural transformation in `app` we can now do everything inside `Eff effs` instead of `Handler`
server :: Members '[SQL, Error ServerError] effs => ServerT API (Eff effs)
server =
       getUsers
  :<|> newUser
