{-# LANGUAGE TemplateHaskell            #-}

module Models where

import Database.Persist.TH
import Data.Aeson
import Data.Aeson.TH

share [mkPersist sqlSettings, mkMigrate "migrateAll"] [persistLowerCase|
User
  name String
  age Int
  UniqueName name
  deriving Show
|]

$(deriveJSON defaultOptions ''User)
