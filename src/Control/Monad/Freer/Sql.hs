{-# LANGUAGE TemplateHaskell #-}
module Control.Monad.Freer.Sql where

import           Database.Persist.Sqlite
import           Database.Sqlite
import           Control.Monad.Freer
import           Control.Monad.Freer.TH
import           Control.Monad.Reader
import qualified Control.Exception             as E

-- | This file shows an example of a SQL effect:

-- | This is the SQL interface. It has two "actions": Sql & TrySql
-- The `Sql` action is for when an error is generally not expected (e.g. select queries)
-- `TrySql` is when an error is to be expected and you want to communicate that error to the user (e.g. unique inserts, see Handler.hs)
data SQL r where
  Sql ::ReaderT SqlBackend IO a -> SQL a
  TrySql ::ReaderT SqlBackend IO a -> SQL (Either SqliteException a)

-- | Automatically generates these functions:
-- sql :: Member SQL effs => ReaderT SqlBackend IO a -> Eff effs a
-- trySql :: Member SQL effs => ReaderT SqlBackend IO a -> Eff effs (Either SqliteException a)
-- (more info: https://hackage.haskell.org/package/freer-simple-1.2.1.0/docs/Control-Monad-Freer-TH.html)
makeEffect ''SQL

-- | Defines an interpreter for the SQL interface above. Passes a SqlBackend that's created on app start (in Api.hs).
runSql
  :: forall a effs
   . Members '[IO] effs
  => SqlBackend
  -> Eff (SQL ': effs) a
  -> Eff effs a
runSql backend = interpret $ handler
 where
  handler :: SQL ~> Eff effs
  handler (Sql    query) = send $ runSqlConn query backend
  handler (TrySql query) = send $ E.try (runSqlConn query backend)